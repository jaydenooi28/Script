import re
import logging
import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def login(driver, username, password, max_retries=3):
    retry_count = 0
    login_url = "http://mail.esmtt.com/src/login.php"

    while retry_count < max_retries:
        try:
            # Reload the login page
            driver.get(login_url)

            # Try to locate the username and password input fields
            input_username = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.NAME, "login_username")))
            input_password = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.NAME, "secretkey")))

            # If located, break out of the loop
            break

        except (TimeoutException, NoSuchElementException) as e:
            logger.warning(
                f"Retry {retry_count + 1}/{max_retries}: Unable to locate username and password input fields. Reloading...")

        retry_count += 1

    if retry_count == max_retries:
        logger.error("Maximum retries reached. Unable to locate username and password input fields.")
    else:
        logger.info("Login fields located. Attempting to log in.")

    # Enter username and password
    input_username.send_keys(username)
    input_password.send_keys(password)
    input_password.submit()


def click_link(keyword):
    driver.get("http://mail.esmtt.com/src/right_main.php?PG_SHOWALL=1&amp;use_mailbox_cache=0&amp;startMessage=1&amp;mailbox=INBOX")
    xpath_locator = f"//*[@title and contains(@title, '{keyword.pattern}')]"
    try:
        # Find all elements that match the pattern
        elements = driver.find_elements(By.XPATH, xpath_locator)

        # Extract and store the href values in a list
        href_values = [element.get_attribute("href") for element in elements]
        return href_values

    except Exception as e:
        print(f"An error occurred: {e}")


def download_file_and_delete_mail(href_values, pattern):
    downloaded_file = []
    try:
        # Wait for element to be present
        for href_url in href_values:
            driver.get(href_url)
            link_elements = WebDriverWait(driver, 10).until(
                EC.presence_of_all_elements_located((By.PARTIAL_LINK_TEXT, pattern.pattern))
            )
            for link_element in link_elements:
                # Prevent duplicate file download
                if link_element.text not in downloaded_file:
                    link_elements[0].click()
                    downloaded_file.append(link_element.text)

                delete_links = driver.find_elements(By.XPATH, "//a[contains(text(), 'Delete') and not("
                                                              "normalize-space(text()) != 'Delete')]")
                delete_links[0].click()

    except Exception as e:
        print(f"An error occurred: {e}")


# Declare Values
id = "wsooi@esmtt.com"
pw = "@qwe@123"
subject_keyword = re.compile(r"<Prod> [WD TSMT B2B] TLOG")
file_link_keyword = re.compile(r"xls")


# Set up the WebDriver and log in
service = Service(executable_path="chromedriver.exe")
driver = webdriver.Chrome(service=service)
login(driver, id, pw)
href_values = click_link(subject_keyword)

# Call this function to download the files
download_file_and_delete_mail(href_values, file_link_keyword)

# Close the current browser session
time.sleep(3)
driver.quit()
