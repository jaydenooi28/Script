import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from time import sleep

url = "https://www.saucedemo.com/"

username = "standard_user"
password = "secret_sauce"


def login_script(uid, pw):
    # Leave the browser open after code complete run
    options = Options()
    options.add_experimental_option("detach", True)
    # --headless allow script to run in the background
    # options.add_argument("--headless")

    driver = webdriver.Chrome(options=options)
    driver.get(url)
    driver.find_element("id", "user-name").send_keys(uid)
    driver.find_element("id", "password").send_keys(pw)
    driver.find_element("id", "login-button").click()
    time.sleep(2)
    if driver.current_url == url:
        print("Login failed")
    else:
        print("Login successful")
    driver.quit()


login_attempt = 0
while login_attempt < 3:
    login_script(username, password)
    login_attempt += 1
    print(f"Login attempt {login_attempt}")
