from create_user import NewUser, input_fields
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


# command to convert to exe file
# python -m PyInstaller --onefile .\register.py


def register_account(uid, input_field, url):
    success = 0
    fail = 0
    options = Options()
    # --headless = selenium runs in the background
    # options.add_argument("--headless")
    # pop out window and run and prevent window from closing
    options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(options=options)
    driver.get(url)
    try:
        # input information here
        for field in input_field:
            if field == "confirm":
                driver.find_element("id", "input-confirm").send_keys(uid.password)
            else:
                driver.find_element("id", f"input-{field}").send_keys(getattr(uid, field))

        # tick the checkbox
        driver.find_element("name", "agree").click()

        # after input click continue/submit
        driver.find_element("css selector", "input[type='submit']").click()

        if driver.current_url == "https://tutorialsninja.com/demo/index.php?route=account/success":
            print("Register successful")
            success += 1
        else:
            print("Register fail")
            fail += 1
    # except Exception as e:
    #     print(f"Error: {str(e)}")
    #     fail_count += 1
    finally:
        pass
        # driver.quit()
    # return success, fail


link = "https://tutorialsninja.com/demo/index.php?route=account/register"

# loop to create user
success_total, fail_total = 0, 0
# from 1 to 10
# for i in range(17, 20):
#     user = NewUser(f"first_{i}", f"last_{i}", f"{i}00{i}")
#     print(f"Attempt Number {i}")
#     success, fail = register_account(user, input_fields, link)
#     success_total += success
#     fail_total += fail
# total_attempt = success_total + fail_total
# print("=============================================================")
# print(f"Total Successful registrations: {success_total}")
# print(f"Total Failed registrations: {fail_total}")
# print(f"Total Attempt: {total_attempt}")
# print("=============================================================")

# for 1 time use
# already create i=1 to 15, avoid using
i = 1
user = NewUser(f"efirst_{i}", f"elast_{i}", f"{i}00{i}")
register_account(user, input_fields, link)
