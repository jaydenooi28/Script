class NewUser:

    def __init__(self, firstname, lastname, telephone):
        self.firstname = firstname
        self.lastname = lastname
        self.email = f"{firstname}.{lastname}@test.com"
        self.telephone = telephone
        self.password = f"{firstname}.{lastname}"


# example i = 19,
# first name: first_19
# last name: last_19
# password: first_19.last_19
# email: first_19.last_19@test.com
# telephone num: 190019

input_fields = ["firstname", "lastname", "email", "telephone", "password", "confirm"]


jay = NewUser("jay", "patel", "190019")