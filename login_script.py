from selenium import webdriver
from selenium.webdriver.chrome.options import Options

url = "https://www.saucedemo.com/"
username = ["standard_user"]

# for testing loop username
# expected result: All login pass except locked_out_user
# username = ["standard_user", "locked_out_user", "problem_user", "performance_glitch_user", "error_user", "visual_user"]
password = "secret_sauce"


def login_script(uid, pw):
    options = Options()
    # select one
    # 1. Leave the browser open after code complete run
    options.add_experimental_option("detach", True)
    # 2. --headless allow script to run in the background
    # options.add_argument("--headless")

    driver = webdriver.Chrome(options=options)
    driver.get(url)
    driver.find_element("id", "user-name").send_keys(uid)
    driver.find_element("id", "password").send_keys(pw)
    driver.find_element("id", "login-button").click()
    if driver.current_url == url:
        print("Login failed")
    else:
        print("Login successful")
    # use this function when testing looping
    # driver.quit()


# choice no. 1 testing one time login
login_script(username, password)

# choice no. 2 testing repeat login with one username
# login_attempt = 0
# while login_attempt < 3:
#     login_attempt += 1
#     print(f"Login attempt {login_attempt}")
#     login_script(username, password)

# test looping the username
# for i in username:
#     print(f"Testing for user: {i}")
#     login_script(i, password)


